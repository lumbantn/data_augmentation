# Data augmentation for discbracket treebanks

## Description
This project contains a class to represent discontinuous constituency trees and a script to artificially augment a training corpus of constituency trees in discbracket format.


## Installation
This project requires [Python 3.6](https://www.python.org/downloads/).
This project requires the installation of a Anaconda environment as explained in [mtgpy](https://gitlab.com/mcoavoux/mtgpy).

## Usage
For details on how to run the program, execute the following command:
```sh
python augment.py --help
```

## Acknowledgements
The source codes in this project uses and heavily borrows from libraries in:
- [mtgpy](https://gitlab.com/mcoavoux/mtgpy-release-findings-2021) : [BERT-Proof Syntactic Structures: Investigating Errors in Discontinuous Constituency Parsing](https://aclanthology.org/2021.findings-acl.288) (Coavoux, Findings 2021)
- [Python NLTK library](https://www.nltk.org/) :  Steven Bird, Ewan Klein, and Edward Loper (2009). [Natural Language Processing with Python](https://www.nltk.org/book/). 

All the experiments in this project uses the mtgpy parser trainer.