"""
DiscoTree class

adapted from mtgpy and nltk.tree
"""

from copy import copy, deepcopy
from nltk.tree import *
from random import choice

class DiscoTree:
    # taken from mtgpy-master/src/tree.py
    def __init__(self, label, children, span=None):
        self.label = label
        self.children = sorted(children, key = lambda x: min(x.get_span()))
        if isinstance(span, int):
            self.span = {span}
        elif isinstance(span, set):
            self.span = span
        else:
            self.span = {i for c in self.children for i in c.get_span()}

    def is_leaf(self):
        return len(self.children) == 0

    # taken from mtgpy-master/src/tree.py
    def get_span(self):
        return self.span
    
    def set_span(self, span):
        self.span = span

    def update_span(self):
        for c in self.get_children():
            c.update_span()
        if not self.is_leaf():
            self.span = {i for c in self.children for i in c.get_span()}

    def get_label(self):
        return self.label
    
    def set_label(self, label):
        self.label = label

    def get_children(self):
        return self.children

    # taken from mtgpy-master/src/tree.py with modifications
    def __str__(self):
        if self.is_leaf():
            return "{}={}".format(min(self.span), self.label)
        else:
            return "({} {})".format(self.label, " ".join([str(c) for c in self.children]))

    def __len__(self):
        return len(self.children)

    @classmethod
    def fromNLTKtree(cls, t):
        if isinstance(t, str):
            index,label = t.split("=",1)
            return cls(label, [], int(index))
        elif isinstance(t, Tree):
            label = t.label()
            children = [DiscoTree.fromNLTKtree(st) for st in t]
            return cls(label, children)

    @classmethod
    def fromstring(cls, s, brackets='()'):
        nltk_tree = Tree.fromstring(s, brackets=brackets)
        return DiscoTree.fromNLTKtree(nltk_tree)

    # taken from nltk.tree with minor modifications
    def __getitem__(self, index):
        if isinstance(index, int):
            return self.children[index]
        elif isinstance(index, (list, tuple)):
            if len(index) == 0:
                return self
            elif len(index) == 1:
                return self[index[0]]
            else:
                return self[index[0]][index[1:]]
        else:
            raise TypeError(
                "%s indices must be integers, not %s"
                % (type(self).__name__, type(index).__name__)
            )
    
    # taken from nltk.tree with minor modifications
    def __setitem__(self, index, value):
        if isinstance(index, int):
            if type(value) == type(self.children[index]):
                self.children[index] = value
                return self
            else:
                raise TypeError("Value type must be the same as current object ({}).".format(type(self.children[index]).__name__))
        elif isinstance(index, (list, tuple)):
            if len(index) == 0:
                raise IndexError('The tree position () may not be ' 'assigned to.')
            elif len(index) == 1:
                self[index[0]] = value
            else:
                self[index[0]][index[1:]] = value
        else:
            raise TypeError(
                "%s indices must be integers, not %s"
                % (type(self).__name__, type(index).__name__)
            )
    
    # taken from mtgpy-master/src/extract_discontinuous_trees.py
    def has_discontinuity(self):
        if self.is_leaf():
            return False
        indexes = sorted(self.span)
        if indexes != [i for i in range(min(indexes), max(indexes) + 1)]:
            return True
        return any([c.has_discontinuity() for c in self.children])
    
    def is_discontinuous(self):
        indexes = sorted(self.span)
        if indexes != [i for i in range(min(indexes), max(indexes) + 1)]:
            return True
        return False
    
    def get_discontinuous_indexes_rec(self, lst, pos):
        if self.is_discontinuous():
            lst.append(pos)
        c = self.get_children()
        for i in range(len(c)):
            newpos = copy(pos)
            newpos.append(i)
            c[i].get_discontinuous_indexes_rec(lst, newpos)

    def get_discontinuous_indexes(self):
        lst = []
        pos = []
        self.get_discontinuous_indexes_rec(lst, pos)
        return lst

    # taken from mtgpy-master/src/extract_discontinuous_trees.py
    # get_list_of_terminals()
    # with minor modification
    def get_leaves(self, lst) :
        if self.is_leaf() :
            lst.append(self)
        for c in self.children :
            c.get_leaves(lst)

    def pretty_string(self):
        lst = []
        self.get_leaves(lst)
        lst.sort(key=lambda x: list(x.get_span())[0])
        tok = [x.get_label() for x in lst]
        return " ".join(tok)

    def pretty_print(self):
        print(self.pretty_string(), "\n")

    def get_positions_of_label_rec(self, label, lst, pos, sub=False):
        if sub:
            if not self.is_leaf() and self.get_label().split("-")[0] == label.split("-")[0]:
                lst.append(pos)
        else:
            if not self.is_leaf() and self.get_label() == label:
                lst.append(pos)
        c = self.get_children()
        for i in range(len(c)):
            newpos = copy(pos)
            newpos.append(i)
            c[i].get_positions_of_label_rec(label, lst, newpos)

    def get_positions_of_label(self, label, sub=False):
        lst = []
        pos = []
        self.get_positions_of_label_rec(label, lst, pos, sub)
        return lst
    
    def get_random_label_position(self, label, sub=False):
        lst = self.get_positions_of_label(label, sub)
        if len(lst) == 0:
            return None
        pos = choice(lst)
        return pos

    def modify_index(self, start, shift):
        if self.is_leaf():
            oldspan = min(self.get_span())
            if oldspan > start:
                newspan = oldspan + shift
                self.span = {newspan}
        else:
            for c in self.get_children():
                c.modify_index(start,shift)

    def substitute(self, index, subtree):
        tree = subtree.clone()

        oldlen = len(self[index].get_span())
        oldmax = max(self[index].get_span())
        oldmin = min(self[index].get_span())
        newlen = len(tree.get_span())
        newmin = min(tree.get_span())
        shift = newlen - oldlen

        # fix insert phrase span
        tree_leaves = []
        tree.get_leaves(tree_leaves)
        tree_leaves.sort(key = lambda x : min(x.get_span()))
        for c in tree_leaves:
            newspan = min(c.get_span()) - newmin + oldmin
            c.set_span({newspan})

        # fix self span
        self.modify_index(oldmax, shift)
        
        # insert new phrase
        self[index] = tree

        # update span
        self.update_span()
    
    def sub_with_check(self, index, tree):
        # needs handling?
        assert self[index].get_label() == tree.get_label()
        self.substitute(index, tree)
    
    def clone(self):
        return DiscoTree.fromstring(str(self))

    # unsure - TBD
    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()