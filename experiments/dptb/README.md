# Experiments : DPTB

## Description
These experiments were done using the Discontinuous Penn Treebank (Evang and Kallmeyer, 2011). There are 4 experiments:
- `dptb` : Base DPTB with no augmentations and not using BERT
- `dptb-bert` : Base DPTB with no augmentations and with BERT
- `dptb-NPaug-p100` : DPTB augmented with 100% generated phrases (doubled) and not using BERT
- `dptb-NPaug-p100-bert` : DPTB augmented with 100% generated phrases (doubled) and with BERT

### <b>Warning: mtgpy version</b>
This experiment uses an older version of the mtgpy trainer. In this experiment, the model selection takes into account only the global f-score and not the discontinuous f-score. 
This is the hash of the git commit for the version used in this experiment:
`a99c9bec8c2f4eb90a26fe7e8f567adc1f568bdb`

## Augmenting the corpus
The training corpus was augmented using the option `--disco-only`. The substitutions were done on NP substructures. Here is the command line that can be used to replicate the augmentation:
```sh
python augment.py --disco-only --label NP -p 1 <input-file> <output-file>
```

## Running the experiment
<b>Note : if running experiments on a GPU, the option `--gpu 0` is required.</b>

In order to launch the experiment, execute these command lines:
```sh
# for experiments not using BERT:
python mtg.py train -c 64 -C 256 --dcu 0.2 -B 8 -w 300 --dwu 0.2 --diff 0.3 -l 0.0015 <model> <train> <dev>

# for experiments using BERT:
python3 mtg.py train --bert --no-word -c 64 -C 128 --dcu 0.2 -B 32 --diff 0.3 -l 0.00006 <model> <train> <dev>
```

## Results
Each folder contains the logs of the experiments in the `.stderr` and `.stdout` files and the best f-score obtained in tests on the dev corpus during training (`best_dev_score`). Due to size constraints, not all the pretrained models are uploaded.

### Pretrained models
The list of pretrained models that are uploaded can be found in the experiments/models directory.
