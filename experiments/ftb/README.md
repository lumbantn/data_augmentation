# Experiments : FTB

## Description
These experiments were done using the [French Treebank](http://www.llf.cnrs.fr/en/Gens/Abeille/French-Treebank-fr.php). These set of experiments were done to see the effect of augmenting the corpus with only phrases containing discontinuities on the discontinuous F-score. The French Treebank was chosen in particular due to the low amount of discontinuities in the original corpus.

### <b>Warning: mtgpy version</b>
This experiment uses a slightly different version of the mtgpy trainer. In this experiment, the model selection takes into account both the f-score and the discontinuous f-score. 
This is the hash of the git commit for the version used in this experiment:
`538e07ede3a1d222a4b84e0c8c33573c85222225`

## Augmenting the corpus
The training corpus was augmented using the options `--disco-only` and `--no-singles`. The substitutions were done on NP substructures. Here is the command line that can be used to replicate the augmentation:
```
python augment.py --disco-only --no-singles --label NP -p {0.2,0.4,...} <input-file> <output-file>
```

### Augmentation results
Here are statistics on the amount of discontinuous phrases found in the original and augmented corpora:

![](ftb_aug_res.png)

## Running the experiment
<b>Note : if running experiments on a GPU, the option `--gpu 0` is required.</b>
### Training
<b><i>Note : due to GPU memory constraints, the batch size (`-B`) in these experiments was reduced from 32 to 16.</b></i>

In order to launch the experiment, execute this command line:
```sh
python3 mtg.py train --bert --no-word -c 64 -C 128 --dcu 0.2 -B 16 --diff 0.3 -l 0.00006 <model> <train> <dev>
```

### Evaluation
In this experiment, we also did an evaluation of a tokenized test corpus using the trained model. Here is the command line :
```
python3 mtg.py eval --gold <gold-discbracket-file> <model> <test-tokens-file>
```
In this case, the `<gold-discbracket-file>` would be `test.discbracket` from `disco_ftb`, the `<model>` is the folder containing the trained model, and the `<test-tokens-file>` is `test.tokens` from `disco_ftb`.

## Results
Each folder contains the logs of the experiments in the `.stderr` and `.stdout` files and the best f-score obtained in tests on the dev corpus during training (`best_dev_score`). The results of the evaluation are written in the `eval_log` file and the evaluation by [`discodop`](https://github.com/andreasvc/disco-dop) version `0.5.2` is in `discodop_eval_log` (and `discodop_disconly_eval_log` for discontinuous only evaluation). Due to size constraints, not all the pretrained models are uploaded.

### Pretrained models
The list of pretrained models that are uploaded can be found in the experiments/models directory.

### Conclusions
At first glance, it seems that the augmentation has improved the model's discontinuous f-score. In the logs, we can see a significant improvement of roughly 10 points from the original disc f-score on the dev corpus.`ftb-base`'s disc f-score was `55.74` and the best model (`ftb-NPaug-p40`) was `67.69`. There is also no decrease to the global f-score of all the models. 

However, upon evaluation on the test corpus, there was no significant improvement to be found. `ftb-base`'s disc f-score was `40.00` and the best model (`ftb-NPaug-p40`) was `41.51`. 

Below are the results of the evaluations on the dev and test corpus:

<i>*the best results are shown in <b>bold green</b></i>

![](ftb_dev_results.png)
![](ftb_test_results.png)