# Experiments

## Description
All the experiments were done on LIG (Laboratoire d'Informatique de Grenoble) servers. Training of the parser model uses the [mtgpy](https://gitlab.com/mcoavoux/mtgpy-release-findings-2021) trainer. All packages and environments required along with the mtgpy parser trainer must be installed in order to replicate the experiments.

## Hyperparameters
These parameters are the same for every experiment. They are inputted as optional arguments in the command line when running the trainer. These best-configuration parameters were taken from: [BERT-Proof Syntactic Structures: Investigating Errors in Discontinuous Constituency Parsing (Coavoux, Findings 2021)](https://aclanthology.org/2021.findings-acl.288/). All optional parameters not written here use their default values `(cf. mtg.py train --help)`.

<b>Note : if running experiments on a GPU, the option `--gpu 0` is required.</b>

### Experiments not using BERT
- Character embeddings dimension `(-c)` : 64
- Character bi-LSTM dimension `(-C)` : 256
- Batch size `(-B)` : 8
- Word embeddings dimension `(-w)` : 300
- Learning rate `(-l)` : 0.0015
- Dropout characters `(--dcu)` : 0.2
- Dropout words `(--dwu)` :  0.2
- Dropout for FF output layers `(--diff)` : 0.3

### Experiments using BERT
- Character embeddings dimension `(-c)` : 64
- Character bi-LSTM dimension `(-C)` : 128
- Batch size `(-B)` : 32
- Learning rate `(-l)` : 0.00006
- Dropout for FF output layers `(--diff)` : 0.3
- `--bert` to use a pretrained BERT model
- `--no-word` to deactivate word embeddings